cmake_minimum_required(VERSION 3.3)

project(bingrep_lib VERSION 1.0.0 DESCRIPTION "Bingrep library.")

add_library(bingrep SHARED
            src/sources/binfinder.cpp
            src/sources/dirtreereader.cpp)

target_include_directories(bingrep PRIVATE src/headers)
