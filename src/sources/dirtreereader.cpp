#include "dirtreereader.h"


/////////////////////////////////
// Constructors
DirTreeReader::DirTreeReader(const std::string& fpath, int typeflag/* = FTW_DEPTH | FTW_PHYS*/)
: path(fpath), typeFlag(typeflag)
{}

DirTreeReader::DirTreeReader(const DirTreeReader& rdt)
{
	path = rdt.path;
	typeFlag = rdt.typeFlag;
	pThis = new DirTreeReader(rdt.path, rdt.typeFlag);
		
}

DirTreeReader::DirTreeReader(DirTreeReader&& rdt)
{
	std::swap(path, rdt.path);
	std::swap(typeFlag, rdt.typeFlag);
	std::swap(pThis, rdt.pThis);
}

DirTreeReader::~DirTreeReader()
{}


/////////////////////////////////
// Static members initialization
DirTreeReader* DirTreeReader::pThis = nullptr;


/////////////////////////////////
// Operator overloads
DirTreeReader& DirTreeReader::operator=(DirTreeReader&& rdt)
{
	if (this == &rdt) return *this;
	
	std::swap(path, rdt.path);
	std::swap(typeFlag, rdt.typeFlag);
	std::swap(pThis, rdt.pThis);
	
	return *this;
}

DirTreeReader& DirTreeReader::operator=(const DirTreeReader& rdt)
{
	if (this == &rdt) return *this;
	
	path = rdt.path;
	typeFlag = rdt.typeFlag;
	pThis = new DirTreeReader(rdt.path, rdt.typeFlag);
	
	return *this;
}


/////////////////////////////////
// Methods
/**
 * @brief Explores a directory tree wrapping nftw. No loads files nor directories.
 * @param fpath Path from where start the exploration.
 * @param noFd Number of file descriptor to hold simultaneously open.
 * @return Same values that nftw, 0 on success, -1 on error.
*/
int DirTreeReader::explore(int noFd/* = 20*/) const
{
	return nftw(path.c_str(), showInfo, noFd, typeFlag);
}

/**
 * @brief Loads all the files into the member vector 'files'.
 * @param noFd Number of the simultaneous open file descriptors.
 * @return Returns a reference to the vector 'files'.
*/
const std::vector<std::string>& DirTreeReader::loadFiles(int noFd/* = 20*/)
{
	pThis = this;
	nftw(path.c_str(), getFiles, noFd, typeFlag);
	
	return files;
}

/**
 * @brief Loads all the directories into the member vector 'dirs'.
 * @param noFd Number of the simultaneous open file descriptors.
 * @return Returns a reference to the vector 'dirs'.
*/
const std::vector<std::string>& DirTreeReader::loadDirs(int noFd/* = 20*/)
{
	pThis = this;
	nftw(path.c_str(), getDirs, noFd, typeFlag);
	
	return dirs;
}

/**
 * @brief Tells the quantity of files scanned.
 * @return Returns the number of the total files loaded to the vector 'files'.
*/
int DirTreeReader::getTotalFiles() const
{
	return files.size();
}

/**
 * @brief Tells the quantity of directories scanned.
 * @return Returns the number of the total files loaded to the vector 'dirs'.
*/
int DirTreeReader::getTotalDirs() const
{
	return dirs.size();
}

/**
 * @brief Gets all the files in the 'fpath' directory recursively.
 * @param fpath Path where start to search recursively.
 * @return Returns on success, non-zero on error. The error is propagated to its caller.
*/
int DirTreeReader::getFiles(const char *fpath, const struct stat *sb, int typeflag, struct FTW *ftwBuff)	// static
{
	// If the element is a file, load it into the file vector
	if (typeflag == FTW_F)
		pThis->files.push_back(fpath);
	
    return 0;	// To tell nftw() to continue
}

/**
 * @brief Gets all the directories in the 'fpath' directory recursively.
 * @param fpath Path where start to search recursively.
 * @return Returns on success, non-zero on error. The error is propagated to its caller.
*/
int DirTreeReader::getDirs(const char *fpath, const struct stat *sb, int typeflag, struct FTW *ftwBuff)	// static
{
	// If the element is a directory, load it into the directory vector
	if (typeflag == FTW_D)
		pThis->dirs.push_back(fpath);
	
    return 0;	// To tell nftw() to continue
}

/**
 * @brief 'nftw's' or 'explore's' callback.
 * @param fpath Path from where start the exploration.
 * @param sb Is a pointer to the stat structure returned by a call to stat(2) for fpath.
 * @param typeflag Is an integer to define the function behavior.
 * @param ftwBuff ORring to define the fuction behavior.
 * @return Returns on success, non-zero on error. The error is propagated to its caller.
*/
int DirTreeReader::showInfo(const char *fpath, const struct stat *sb, int typeflag, struct FTW *ftwBuff)	// static
{	/**
	 * Replace the print sentenses for some C++ out (cout)
	*/
	printf("%-3s %2d %7jd   %-40s %d %s\n",
	(typeflag == FTW_D) ?   "d"   : (typeflag == FTW_DNR) ? "dnr" :
	(typeflag == FTW_DP) ?  "dp"  : (typeflag == FTW_F) ?   "f" :
	(typeflag == FTW_NS) ?  "ns"  : (typeflag == FTW_SL) ?  "sl" :
	(typeflag == FTW_SLN) ? "sln" : "???",
	ftwBuff->level, (intmax_t) sb->st_size,
	fpath, ftwBuff->base, fpath + ftwBuff->base);
	
    return 0;	// To tell nftw() to continue
}
