#include "binfinder.h"
#include <cstring>
#include <iostream>


#define MBLOCK_SIZE 67108864	// 64 MiB
//#define MBLOCK_SIZE 2	//debug


/////////////////////////////////
// Constructors
BinFinder::BinFinder(const std::string filePath)
: buff(new char[MBLOCK_SIZE]), comp(comp.reset()), path(filePath), file(), flog()
{}

BinFinder::BinFinder(BinFinder&& bf)
{
	buff = bf.buff;
	bf.buff = nullptr;
	std::swap(comp, bf.comp);	//comp |= bf.comp;	// Revisar si swapea correctamente
	std::swap(path, bf.path);
}

BinFinder::BinFinder(const BinFinder& bf)
{
	buff = new char[MBLOCK_SIZE];
	for (auto i = 0; i < MBLOCK_SIZE; ++i)
		buff[i] = bf.buff[i];
	
	comp |= bf.comp;
	path = bf.path;
	//file();
	//flog();
}

BinFinder::~BinFinder()
{
	delete[] buff;
	file.close();
	flog.close();
}


/////////////////////////////////
// Operator overloads
BinFinder& BinFinder::operator=(BinFinder&& bf)
{
	if (this == &bf) return *this;
	
	buff = bf.buff;
	bf.buff = nullptr;
        std::swap(comp, bf.comp);
	std::swap(path, bf.path);
	
	return *this;
}

BinFinder& BinFinder::operator=(const BinFinder& bf)
{
	if (this == &bf) return *this;
	
	buff = new char[MBLOCK_SIZE];
	for (auto i = 0; i < MBLOCK_SIZE; ++i)
		buff[i] = bf.buff[i];
	
	comp |= bf.comp;
	path = bf.path;
	//file();
	//flog();
	
	return *this;
}


/////////////////////////////////
// Methods
/**
 * @brief Search for a parcial or whole expression in a file while the missing expressions are less or equal to 'fails'.
 * @param vecBin A vector container with the expressions to find.
 * @param fails Number of the fails allowed.
 * @return Returns the offset on success, and -1 on error.
*/
int BinFinder::containsInFile(const std::vector<std::bitset<8>>& vecBin, const uint failsAllowed/* = 1*/, const bool createLog/* = false*/)
{
	if (failsAllowed >= vecBin.size()) return -1;
	
	file.open(path, std::ifstream::binary);
	if ((file.rdstate() & std::ifstream::failbit ) != 0)
		return -1;
	
	bool matching {}, eob {};	// eob = End of Block
	int found {}, vecIndex {}, offset { -1 }, blockIterations {};
	uint bytesRead { 1 }, nextPos {}, failed {}, j {}, searchVecIndex {};
	
	// Reads the file
	while (!file.eof() && (!found || matching) && bytesRead > 0)
	{
		// Reading blocks until MBLOCK_SIZE Bytes
		bytesRead = file.rdbuf()->sgetn(buff, MBLOCK_SIZE);
		if (bytesRead && eob) { eob = false; ++blockIterations; }
		
		if (!matching) vecIndex = 0;
		
		// Iterates the vector
		for (uint i = vecIndex; i < vecBin.size() && bytesRead/* && !eob*/; ++i)
		{
			// Iterates the block of memory
			for (j = nextPos; j < bytesRead; ++j)
			{
				comp.reset();
				comp |= buff[j];
				if (createLog) log(vecBin, i, j, blockIterations);
				if (vecBin.at(i) == comp)
				{
					if (offset < 0)
					{
						if (blockIterations == 1)
							offset = j;
						else
							offset = j + (MBLOCK_SIZE * blockIterations);
						searchVecIndex = i;
					}
					
					//  If less patterns left to find than the fails allowed
					if (((vecBin.size() - 1) - i) + failed <= failsAllowed)
					{
						file.close();
						return offset;
					}
					
					found = 1;
					matching = true;
					nextPos = j + 1;
				}
				else
				{
					if (matching)
					{
						++failed;
						nextPos = j + 1;
						if (failed > failsAllowed)
						{
							found = 0;
							matching = false;
							i = searchVecIndex;	// Restores the index vector position from where to continue
							j = offset;	// Continue searching from the next position where the first expression was found. With the next iteration, j=j+1
							offset = -1;
							failed = 0;
						}
					}
				}
				
				if (found && i < vecBin.size() - 1)
					break;
				else if (found && i >= vecBin.size() - 1)
					i = 0;
			}
			
			// End of Block
			if (j >= bytesRead)
			{
				eob = true;
				nextPos = 0;
				++vecIndex;	// Look for the next pattern
				
				// Matching between blocks
				if (matching)
				{
					if (file.tellg() == sizeOfFile())
					{
						file.close();
						return -1;
					}
					
					vecIndex = i;
					break;
				}
				
				// If a search fails, and the fails allowed is < than the number of it fails, go for the next block
				if (!found && (failsAllowed < (vecBin.size() - 1) - i)) break;
			}
		}
	}
	
	file.close();
	if (found)
		return offset + (MBLOCK_SIZE * blockIterations);
	else
		return -1;
}

/**
 * @brief Returns the size of the open file.
 * @return Size of the open file. -1 on error.
*/
int BinFinder::sizeOfFile()
{
	if (!file.is_open()) return -1;
	
	int curr = file.tellg();
	file.seekg(0, file.end);
	int size = file.tellg();
	file.seekg(curr);
	
	return size;
}

/**
 * @brief Sets a new path file where find the expression.
 * @param newPath New path to the file.
 */
void BinFinder::setFile(const std::string& newPath)
{
	path = newPath;
}

/**
 * @brief Logs the comparisons.
 * @param vec The outside vector with the patterns.
 */
void BinFinder::log(const std::vector<std::bitset<8>>& vec, const int i, const int j, const int blckIt)
{
	if (!flog.is_open())
		flog.open("log.txt");
	flog << "Comparing " << std::uppercase << std::hex << "0x" << vec.at(i).to_ulong() << " : " << "0x" << comp.to_ulong()
	<< "\t| ASCII: " << static_cast<char>(vec.at(i).to_ulong()) << " : " << static_cast<char>(comp.to_ulong())
	<< "\t| Binary: " << vec.at(i) << " : " << comp
	<< "\t| offset: " << std::hex << "0x" << j + (MBLOCK_SIZE * blckIt) << std::endl;
}
